%the required 6 files categories:
%              specs    simple    combined
% - positives    1       1        1
% - false d's  (x32)   (x32)    (x32) 
 

%todo:
%   rename files correctly

function [] = features_fun_all_chunks(night_index)
%%%%%%%%%%%%%%%%%%%%%%
%Extract features
%%%%%%%%%%%%%%%%%%%%%%
% This script extracts DSP/Computer Vision features from segmented spectrograms
%
% Kolbeinn Karlsson, Cornell University
% Nicholas Bruns, Cornell University
% 01/17/13
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRELIMINIARIES: Set path
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Path to spectrogram
pos_spec_path = '/home/fs01/neb76/positive_segs/spec_mats/';
fd_spec_path='/home/fs01/neb76/FULL_false_detection_segs/spec_mats/';
%Path to true positive segmentations, combined then simple
pos_comb_path = '/home/fs01/neb76/positive_segs/combined/';
pos_simp_path = '/home/fs01/neb76/positive_segs/simple/';

%paths to false detection segmentation chunks
fd_comb_path='/home/fs01/neb76/FULL_false_detection_segs/combined/';
fd_simp_path='/home/fs01/neb76/FULL_false_detection_segs/simple/';

%write out paths
out_path='/home/fs01/neb76/FULL_feature_label_sets/';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MORE PRELIMINIARIES: Interpret input
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
night_string=['night_' num2str(night_index)];

pos_spec_path=[pos_spec_path night_string '.mat'];
pos_simp_path = [pos_simp_path night_string '.mat'];
pos_comb_path = [pos_comb_path night_string '.mat'];

fd_spec_path=[fd_spec_path night_string '/'];
fd_simp_path = [fd_simp_path night_string '/'];
fd_comb_path = [fd_comb_path night_string '/'];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART ONE: Extract features from combined segmentation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
load_cell={night_string};
chunks= dir([fd_spec_path '*.mat']);
for i=1:length(chunks)
  cur_chunk_name=chunks(i).name;
  load_cell{end+1}=cur_chunk_name;
end

data = cell(1,2); %Will hold the data when extraction complete
k=1; %use to keep track of storage indeces
%loop through positive specs/segs first, then the ~32 chunks of false detections
for lc=1:length(load_cell)
  %special case for positive segments
  if lc==1
    eval(['load '  pos_spec_path]);
    eval(['load '  pos_comb_path]);
  else
    chunk_pieces= (regexp (load_cell{lc}, '_', 'split'));
    eval(['load '  fd_spec_path load_cell{lc}]);
    eval(['load '  fd_comb_path 'segMat_chunk_' chunk_pieces{3}]);
    segMat=segMat_combined;  
  end

  
  L = size(segMat,1); %Get number of segmented spectrograms

 
  %Loop through spectrograms and extract features
  for i = 1:L
    M = max(max(segMat{i,1}));
    %Loop through every segment in the frame
    for j = 1:M
      %First isolate one object at a time
      seg = segMat{i,1} == j;
      %seg = (segMat{i,1} == j) .* ...
      %  specMat{i,1}(1:size(segMat{i,1},1),:);
      if nnz(seg) == 0
        continue; %Continue if there is no such segment in the frame
      end
      %Otherwise, extract its features
      [row, col] = find(seg);
      duration = max(col) - min(col); %Get duration
      high_freq = min(row);           %Get highest frequency
      low_freq = max(row);            %Get lowest frequency
      area = nnz(seg);                %Get number of pixels
      %Get elliptical shape descriptors
      stats = regionprops(seg,...
        cellstr(['MajorAxisLength';'MinorAxisLength';'Orientation    ']));
      %Now get the actual values from the spectrogram
      %spec_index = segMat{i,2}; neb correction, now all specs line up
      %directly
      spec = seg .* specMat{i,1}(1:size(segMat{i,1},1),:);
      %Get maximum intensity frequency
      [max_intens_freq,~] = find(abs(spec) == max(max(abs(spec))));
      %Get centroid
      [y,x] = find(spec);
      xmom = sum(x .* find(spec)) / sum(spec(spec ~= 0));
      ymom = sum(y .* find(spec)) / sum(spec(spec ~= 0));
      %Translate the x-value
      xmom = xmom - min(col);
      %Now get mean intensity
      mean_intens = mean(spec(spec ~= 0));
      
      %Now that we have all the features, create feature vector
      feat = [duration, high_freq, low_freq, area, ...
        stats(1).MajorAxisLength, stats(1).MinorAxisLength, stats(1).Orientation,...
        max_intens_freq, xmom, ymom, mean_intens];
      %And the label
      if lc==1 && j == 1
        label = 1;
      else
        label = 0;
      end
      
      %Add this to the cell array
      data{k,1} = feat;
      data{k,2} = label;
      k = k+1;
    end
  end

end %end load_cell loop

save([ out_path 'combined/' night_string '.mat'], 'data');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART TWO: Extract features from simple segmentation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Clear the old data and load in the new
clear segMat data

data = cell(1,2); %Will hold the data when extraction complete
k = 1; %Index to keep track of the size of train_data
for lc=1:length(load_cell)
  %special case for positive segments
  if lc==1
    eval(['load ' pos_simp_path]);
  else
    chunk_pieces= (regexp (load_cell{lc}, '_', 'split'));  
    eval(['load ' fd_simp_path 'segMat_chunk_' chunk_pieces{3}]);
    segMat=segMat_simple;  
  end

  L = size(segMat,1); %Get number of segmented spectrograms

  


  %Loop through spectrograms and extract features
  for i = 1:L
    M = max(max(segMat{i,1}));
    %Loop through every segment in the frame
    for j = 1:M
      %First isolate one object at a time
      seg = segMat{i,1} == j;
      %seg = (segMat{i,1} == j) .* ...
      %  specMat{i,1}(1:size(segMat{i,1},1),:);
      if nnz(seg) == 0
        continue; %Continue if there is no such segment in the frame
      end
      %Otherwise, extract its features
      [row, col] = find(seg);
      duration = max(col) - min(col); %Get duration
      high_freq = min(row);           %Get highest frequency
      low_freq = max(row);            %Get lowest frequency
      area = nnz(seg);                %Get number of pixels
      %Get elliptical shape descriptors
      stats = regionprops(seg,...
        cellstr(['MajorAxisLength';'MinorAxisLength';'Orientation    ']));
      %Now get the actual values from the spectrogram
      %spec_index = segMat{i,2};
      spec = seg .* specMat{i,1}(1:size(segMat{i,1},1),:);
      %Get maximum intensity frequency
      [max_intens_freq,~] = find(abs(spec) == max(max(abs(spec))));
      %Get centroid
      [y,x] = find(spec);
      xmom = sum(x .* find(spec)) / sum(spec(spec ~= 0));
      ymom = sum(y .* find(spec)) / sum(spec(spec ~= 0));
      %Translate the x-value
      xmom = xmom - min(col);
      %Now get mean intensity
      mean_intens = mean(spec(spec ~= 0));
      
      %Now that we have all the features, create feature vector
      feat = [duration, high_freq, low_freq, area, ...
        stats.MajorAxisLength(1), stats.MinorAxisLength(1), stats.Orientation(1),...
        max_intens_freq, xmom, ymom, mean_intens];
      %And the label
      if lc==1 && j == 1
        label = 1;
      else
        label = 0;
      end
      
      %Add this to the cell array
      data{k,1} = feat;
      data{k,2} = label;
      k = k+1;
    end
  end

end
%Save the data
save([ out_path 'simple/' night_string '.mat'], 'data');

    
end
    
    
    
    