import sys
import scipy  
import numpy
import pylab
import pickle
import scipy.io as sio
import sklearn.cross_validation as skcv
import pylab as pl
from scipy import interp
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn import svm
from sklearn import metrics
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn import cross_validation as cv

#Here we do leave-one-night-out testing


##################################################
# Create our own function for cross-validating with MCC
##################################################

def matthewscc(cm):
    if len(cm) == 1:
        return 0
    tn = cm[0][0]; fp = cm[0][1]; fn = cm[1][0]; tp = cm[1][1]
    denom = (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn)
    if denom == 0:
        mcc = (tp*tn - fp*fn)
    else:
        mcc = (tp*tn - fp*fn) / numpy.sqrt(denom)
    return mcc

def cross_val_mcc(classifier,features,labels,nfolds):
    n = len(features)
    folder = cv.KFold(n,nfolds)
    mcc_array = list()
    for train_index,test_index in folder:
        #First split into training and test set
        f_train = list()
        f_test = list()
        l_train = list()
        l_test = list()
        for k in train_index:
            f_train.append(features[k])
            l_train.append(labels[k])
        for k in test_index:
            f_test.append(features[k])
            l_test.append(labels[k])
        #Now train the classifier
        #print "Length f_train: ",len(f_train), " l_train: ",len(l_train)
        if max(l_train) != min(l_train):
            classifier.fit(f_train,l_train)
            #Run it on the test set
            l_pred = classifier.predict(f_test)
            #Compute confusion matrix
            cm = metrics.confusion_matrix(l_test,l_pred)
            #Compute MCC
            mcc = matthewscc(cm)
            mcc_array.append(mcc)
    return mcc_array
        
        

##################################################
# Load the Data
##################################################

file = open('features.pickle','r')
mix_features = pickle.load(file)
file.close()

file = open('labels.pickle','r')
mix_labels = pickle.load(file)
file.close()

file = open('features_fdn1.pickle','r')
fdn1_features = pickle.load(file)
file.close()
file = open('labels_fdn1.pickle','r')
fdn1_labels = pickle.load(file)
file.close()

file = open('features_fdn2.pickle','r')
fdn2_features = pickle.load(file)
file.close()
file = open('labels_fdn2.pickle','r')
fdn2_labels = pickle.load(file)
file.close()

file = open('features_fdn3.pickle','r')
fdn3_features = pickle.load(file)
file.close()
file = open('labels_fdn3.pickle','r')
fdn3_labels = pickle.load(file)
file.close()

file = open('features_fdn4.pickle','r')
fdn4_features = pickle.load(file)
file.close()
file = open('labels_fdn4.pickle','r')
fdn4_labels = pickle.load(file)
file.close()

#Add the mixed detections and the false detections together for each night
all_features = list()
all_labels = list()

all_features.append( mix_features[0] + fdn1_features )
all_features.append( mix_features[1] + fdn2_features )
all_features.append( mix_features[2] + fdn3_features )
all_features.append( mix_features[3] + fdn4_features )

all_labels.append( mix_labels[0] + fdn1_labels )
all_labels.append( mix_labels[1] + fdn2_labels )
all_labels.append( mix_labels[2] + fdn3_labels )
all_labels.append( mix_labels[3] + fdn4_labels )

#################################################
# Now the classification 
#################################################

n_nights = len(all_features) #Number of nights

resm_rbf = list()

#Now leave one night out
for i in range(n_nights):
    #Create the test set from the LONO night
    f_test = all_features[i]
    l_test = all_labels[i]
    #Create the training set from the rest
    f_train = list()
    l_train = list()
    for j in range(n_nights):
        if j!=i :
            f_train = f_train + all_features[j]
            l_train = l_train + all_labels[j]
    #Scale/standardize data
    f_train = preprocessing.scale(f_train)
    f_test = preprocessing.scale(f_test)

    #Now we do parameter optimization
        
    #RBF SVM
    print " "

    rbf_errs = list()
    for i in c_vals:
        rbf = svm.SVC(kernel='rbf',C = i)
        rbferr = skcv.cross_val_score(rbf, f_train, l_train, cv=10)
        rbf_errs.append(numpy.mean(rbferr))
        sys.stdout.write('.')
    #Find maximum score
    k = rbf_errs.index(max(rbf_errs))
    rbf = svm.SVC(kernel='rbf',C=c_vals[k])
    rbf.fit(f_train, l_train)
    terr = rbf.score(f_test,l_test)
    print "\n\nRBF SVM"
    print "Test error: ", terr
    #Now find other classification metrics
    l_pred = rbf.predict(f_test)
    cr = metrics.classification_report(l_test,l_pred)
    cm = metrics.confusion_matrix(l_test,l_pred)
    print "\nTrue neg:  ", cm[0][0], " False neg: ", cm[0][1], \
        "\nFalse pos: ", cm[1][0], "True pos: ", cm[1][1]
    print cr
    mcc = matthewscc(cm)
    print "MCC: ", mcc
    trecall = metrics.precision_score(l_test,l_pred)
    tprecision = metrics.recall_score(l_test, l_pred)
    tf1 = metrics.f1_score(l_test,l_pred)
    m = [terr,trecall,tprecision,tf1]
    resm_rbf.append(m)

