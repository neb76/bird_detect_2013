import scipy  
import numpy
import pylab
import pickle
import scipy.io as sio
import sklearn.cross_validation as skcv
import pylab as pl
from scipy import interp
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn import svm
from sklearn.metrics import roc_curve, auc, confusion_matrix, \
    classification_report
from sklearn.cross_validation import StratifiedKFold

##################################################
# Load the Data
##################################################

file = open('features.pickle','r')
all_features = pickle.load(file)
file.close()

file = open('labels.pickle','r')
all_labels = pickle.load(file)
file.close()

feats = all_features[0] + all_features[1] + all_features[2] + \
    all_features[3] + all_features[4]
labs = all_labels[0] + all_labels[1] + all_labels[2] + \
    all_labels[3] + all_labels[4]

###########################################################

#Scale data (standardize it)
feats = preprocessing.scale(feats)

#Get number of features
num_feats = len(feats[0])

##################################################
# RBF Kernel SVM 
##################################################

rbfsvm = svm.SVC(kernel='rbf')
rserr = skcv.cross_val_score(rbfsvm, feats, labs, cv=10)
print "RBF Kernel SVM: ", numpy.mean(rserr)

##################################################
# Parameter Optimization
##################################################

#RBF SVM
c_vals = [0.1, 0.5, 1.0, 1.5, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0]
rbf_errs = list()
for i in c_vals:
    mysvm = svm.SVC(kernel='rbf',C = i)
    acc = skcv.cross_val_score(mysvm, feats, labs, cv=10)
    rbf_errs.append(1 - numpy.mean(acc))

pylab.plot(c_vals,svm_errs)
pylab.xlabel('C')
pylab.ylabel('Error rate')
pylab.title('Cross-val of Linear SVM')
pylab.savefig('Lin_SVM_cv.png')
pylab.show()
