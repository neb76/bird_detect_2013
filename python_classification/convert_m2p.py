import scipy.io as sio
import numpy as np
import pickle

#Set the path
path = './'

#Load the mat file
mat1 = sio.loadmat(path + 'night_1.mat')
mat2 = sio.loadmat(path + 'night_2.mat')
mat3 = sio.loadmat(path + 'night_3.mat')
mat4 = sio.loadmat(path + 'night_4.mat')
mat5 = sio.loadmat(path + 'night_5.mat')

#Extract the data. datan is now a numpy array of numpy arrays
data = list()
data.append(mat1['data'])
data.append(mat2['data'])
data.append(mat3['data'])
data.append(mat4['data'])
data.append(mat5['data'])


#Delete original objects to free up memory
del mat1
del mat2
del mat3
del mat4
del mat5

#Concatenate
#all_data = np.concatenate((data1,data2,data3,data4,data5),0)

#Free more memory
#del data1
#del data2
#del data3
#del data4
#del data5

k=1
for obj in data:
    #Separate features and labels
    features = list()
    labels = list()
    for item in obj:
        features.append(item[0][0]) #Get the features
        labels.append(item[1][0][0]) #Get the label

    #Now pickle the data
    filename = open('features_n' + str(k) + '.pickle','w')
    pickle.dump(features,filename)
    filename.close()

    filename = open('labels_n' + str(k) + '.pickle','w')
    pickle.dump(labels,filename)
    filename.close()

    k = k+1




