import sys
import scipy  
import numpy
import pylab
import pickle
import scipy.io as sio
import sklearn.cross_validation as skcv
import pylab as pl
from scipy import interp
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.lda import LDA
from sklearn.qda import QDA
from sklearn import svm
from sklearn import metrics
from sklearn.cross_validation import StratifiedKFold
from sklearn.cross_validation import train_test_split
from sklearn import cross_validation as cv

##################################################
# Create our own function for cross-validating with MCC
##################################################

#Compute the Matthews Correlation Coefficient from a confusion matrix
def matthewscc(cm):
    tn = cm[0][0]; fp = cm[0][1]; fn = cm[1][0]; tp = cm[1][1]
    denom = (tp+fp)*(tp+fn)*(tn+fp)*(tn+fn)
    if denom == 0:
        mcc = (tp*tn - fp*fn)
    else:
        mcc = (tp*tn - fp*fn) / numpy.sqrt(denom)
    return mcc

#Cross-validate and run 
def cross_val_mcc(classifier,features,labels,nfolds):
    n = len(features)
    folder = cv.KFold(n,nfolds)
    mcc_array = list()
    for train_index,test_index in folder:
        #First split into training and test set
        f_train = list()
        f_test = list()
        l_train = list()
        l_test = list()
        for k in train_index:
            f_train.append(features[k])
            l_train.append(labels[k])
        for k in test_index:
            f_test.append(features[k])
            l_test.append(labels[k])
        #Now train the classifier
        #print "Length f_train: ",len(f_train), " l_train: ",len(l_train)
        if max(l_train) != min(l_train):
            classifier.fit(f_train,l_train)
            #Run it on the test set
            l_pred = classifier.predict(f_test)
            #Compute confusion matrix
            cm = metrics.confusion_matrix(l_test,l_pred)
            #Compute MCC
            mcc = matthewscc(cm)
            mcc_array.append(mcc)
    return mcc_array
        
        

##################################################
# Load the Data
##################################################

file = open('features.pickle','r')
mix_features = pickle.load(file)
file.close()

file = open('labels.pickle','r')
mix_labels = pickle.load(file)
file.close()

file = open('features_fdn1.pickle','r')
fdn1_features = pickle.load(file)
file.close()
file = open('labels_fdn1.pickle','r')
fdn1_labels = pickle.load(file)
file.close()

file = open('features_fdn2.pickle','r')
fdn2_features = pickle.load(file)
file.close()
file = open('labels_fdn2.pickle','r')
fdn2_labels = pickle.load(file)
file.close()

file = open('features_fdn3.pickle','r')
fdn3_features = pickle.load(file)
file.close()
file = open('labels_fdn3.pickle','r')
fdn3_labels = pickle.load(file)
file.close()

file = open('features_fdn4.pickle','r')
fdn4_features = pickle.load(file)
file.close()
file = open('labels_fdn4.pickle','r')
fdn4_labels = pickle.load(file)
file.close()

#Put all the mixed examples together
mix_feats = mix_features[0] + mix_features[1] + mix_features[2] + \
    mix_features[3] + mix_features[4]
mix_labs = mix_labels[0] + mix_labels[1] + mix_labels[2] + \
    mix_labels[3] + mix_labels[4]
num_mix_feats = len(mix_feats)

#Put all the pure negative examples together
neg_feats = fdn1_features + fdn2_features + fdn3_features + fdn4_features
neg_labs = fdn1_labels + fdn2_labels + fdn3_labels + fdn4_labels
num_neg_feats = len(neg_feats)

#####################################################
# Preprocess data
#####################################################

#divide into training set and test set
f_train, f_test, l_train, l_test = train_test_split(
    mix_feats, mix_labs, test_size=0.3)

#divide the pure negative examples up
nf_train, nf_test, nl_train, nl_test = train_test_split(
    neg_feats, neg_labs, test_size=0.3)

#Mix the test sets together
f_test = numpy.concatenate((f_test, nf_test), axis=0)
l_test = numpy.concatenate((l_test, nl_test), axis=0)

#Scale (standardize) the test set
f_test = preprocessing.scale(f_test)

###########################################################
rf_test_error = list()
lsvm_test_error = list()
rbf_test_error = list()
rf_mcc_list = list()
lsvm_mcc_list = list()
rbf_mcc_list = list()
#Now add alpha proportion of negs to the training set
alphas = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

for alpha in alphas:
    k = int(len(nf_train)*alpha)
    features_train = numpy.concatenate((f_train,nf_train[0:k]),axis=0)
    labels_train = numpy.concatenate((l_train,nl_train[0:k]),axis=0)

    #Scale (standardize) training data
    features_train = preprocessing.scale(features_train)

    #Count number of pos and neg examples in test set:
    n_pos = 0
    n_neg = 0
    err = 0
    for i in l_test:
        if i == -1:
            n_neg = n_neg + 1
        elif i == 1:
            n_pos = n_pos + 1

    #Count number of pos and neg examples in train set:
    n_pos2 = 0
    n_neg2 = 0
    err = 0
    for i in labels_train:
        if i == -1:
            n_neg2 = n_neg2 + 1
        elif i == 1:
            n_pos2 = n_pos2 + 1

    print "\nAlpha: ", alpha
    print "Test set size: ", len(l_test)
    print "Positives: ", n_pos
    print "Negatives: ", n_neg
    print "Training set size: ", len(features_train)
    print "Positives: ", n_pos2
    print "Negatives: ", n_neg2

###############################
#Now we run classifiers
###############################

#RBF SVM
    print " "

    rbf_errs = list()
    for i in c_vals:
        rbf = svm.SVC(kernel='rbf',C = i)
        rbferr = skcv.cross_val_score(rbf, features_train, labels_train, cv=10)
        rbf_errs.append(numpy.mean(rbferr))
    #print "RBF SVM, train: ",numpy.mean(rbferr), \
        #    " C: ", i
        sys.stdout.write('.')
#Find maximum score
    k = rbf_errs.index(max(rbf_errs))
    rbf = svm.SVC(kernel='rbf',C=c_vals[k])
    rbf.fit(features_train, labels_train)
    terr = rbf.score(f_test,l_test)
    print "\n\nRBF SVM"
    print "Test error: ", terr
    rbf_test_error.append(terr)
#Now find other classification metrics
    l_pred = rbf.predict(f_test)
    cr = metrics.classification_report(l_test,l_pred)
    cm = metrics.confusion_matrix(l_test,l_pred)
    print "\nTrue neg:  ", cm[0][0], " False neg: ", cm[0][1], \
        "\nFalse pos: ", cm[1][0], "True pos: ", cm[1][1]
    print cr
    mcc = matthewscc(cm)
    print "MCC: ", mcc
    rbf_mcc_list.append(mcc)

##################################################
# Now we plot the results
##################################################

pylab.figure(1)
pylab.plot(alphas,rbf_test_error)
pylab.xlabel('Alpha')
pylab.ylabel('Accuracy rate')
pylab.title('Effect of alpha')
pylab.legend(["RBF"],loc=2)
pylab.savefig('effect_alpha_error.png')



pylab.figure(2)
pylab.plot(alphas,rbf_mcc_list)
pylab.xlabel('Alpha')
pylab.ylabel('MCC')
pylab.title('Effect of alpha')
pylab.legend(["RBF"],loc=2)
pylab.savefig('effect_alpha_mcc.png')


print "\n\n**********************************************"
print "         Done!      "
print "**********************************************\n\n"
