%%%%%%%%%%%%%%%%%%%%%%
%Classify segments
%%%%%%%%%%%%%%%%%%%%%%
% This script runs a RBF-SVM classifier on features extracted by features.m
%
% Kolbeinn Karlsson, Cornell University
% Nicholas Bruns, Cornell University
% 01/17/13
%
% The only parameters that need to be set are in the PRELIMINARIES section.
% Most likely, only the path to the data must be altered. The output of 
% the script is saved to a file called 'birdoutput.txt'.

tic_id = tic; %To measure the time this function takes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PRELIMINIARIES: Set parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%Algorithm parameters%%%%
optimize_parameters = 0;  %Set to true to grid search for best parameters

%%%if optimize_parameters == 1, then set following parameters
k = 10; %The k in k-fold cross-validation
lb = [-5 -5]; %Lower bound for the parameter search
ub = [5 5];   %Upper bound for the parameter search

%%%if optimize_parameters == 0, then set following parameters
rbf_sigma = 1.0; %Scaling parameter for the RBF function
%Box constrains for the SVM. Higher C usually gives precision at the expense
%of recall, while lower C usually gives recall at the expense of precision
C = 1;

%Specify the ratio of the data that should be used for testing
test_ratio = 0.3;  

%%%%Path to data%%%%
%We make a cell array where each entry is a path to an output file from
%the feature extractor in 'features.m'.
paths = cell(1);
paths{1} = './data_simple.mat';
paths{2} = './data_combined.mat';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART ONE: Load data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Now load the data
load(paths{1});
alldata = data;
for i = 2:length(paths)
  load(paths{i});
  alldata = cat(1,alldata,data);
end

%Convert data from cell arrays to matrices for easier manipulation
feats = cell2mat(alldata(:,1));
labels = cell2mat(alldata(:,2))==1; %Convert from double to logical

%No need to keep duplicates of the data
clear data alldata

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART TWO: Divide into training and test set
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

l = length(labels);
random_ind = randperm(l);
d = round(test_ratio*l); %The dividing line between test and train indices
test_ind = random_ind(1:d);
train_ind = random_ind(d+1:end);

%Training set
x_train = feats(train_ind,:);
y_train = labels(train_ind);
%Get length of training data
train_length = size(x_train,1);

%Test set
x_test = feats(test_ind,:);
y_test = labels(test_ind);
%Get length of test data
test_length = size(x_test,1);

clear feats labels %Clear up memory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART THREE: Grid search for best parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%There are two parameters to optimize: boxconstraint (C) and rbf_sigma,
%the RBF scaling function.

if optimize_parameters
  %Now perform cross-validation for parameters
  cvp = cvpartition(train_length,'kfold',k);
  
  %Set up a function that takes an input z=[rbf_sigma,boxconstraint],
  %and returns the cross-validation value of exp(z)
  %(taken from the MATLAB documentation)
  minfn = @(z)crossval('mcr',x_train,y_train,'Predfun', ...
    @(xtrain,ytrain,xtest)crossfun(xtrain,ytrain,...
    xtest,exp(z(1)),exp(z(2))),...
    'partition',cvp);
  
  lb = [-5 -5]; %Lower bound for the parameter search
  ub = [5 5];   %Upper bound
  %Set search tolerances
  opts = optimset('TolX',5e-4,'TolFun',5e-4);
  
  tic
  [x,fval] = patternsearch(minfn,[1 1],[],[],[],[],lb,ub,[],opts);
  toc
  
  %Now we have our answer
  rbf_sigma = exp(x(1));
  C = exp(x(2));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART FOUR: Training
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Now classify the data. Keep in mind that the svmtrain function is set to
%automatically normalize the data (autoscale true), so there is no need to 
%do that beforehand.

%Set optimization parameters
opts = statset('MaxIter',15000,'Robust','on');

svmstruct = svmtrain(x_train,y_train,'kernel_function','rbf',...
  'boxconstraint',C,'autoscale',true,'rbf_sigma',rbf_sigma,...
  'kktviolationlevel',0.1,'options',opts);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART FIVE: Testing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Run the classifier on the test data
y_pred = svmclassify(svmstruct,x_test);

%%%%Now get performance metrics%%%%

%Confusion matrix
cm = confusionmat(y_test,y_pred);
true_neg  = cm(1,1);
false_pos = cm(1,2);
false_neg = cm(2,1);
true_pos  = cm(2,2);

%Precision and recall
precision = true_pos / (true_pos + false_pos);
recall    = true_pos / (true_pos + false_neg);

%F1 score
f1score = 2 * (precision*recall) / (precision+recall);

%Misclassification rate
misclass = (false_pos+false_neg)/sum(cm(:));

%%%%Get data on training and test sets%%%%

%Training set
train_size = length(y_train);
train_pos = sum(y_train==1);
train_neg = sum(y_train==0);

%Test set
test_size = length(y_test);
test_pos = sum(y_test==1);
test_neg = sum(y_test==0);

%Now get the elapsed time
elapsed_time = toc(tic_id);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PART SIX: Write results to file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

file = fopen('birdoutput.txt','w');

fileheader = ['============================================================',...
  '===============\n',...
  'Results of running an RBF SVM on computer vision / DSP ',...
  'features extracted \nfrom long-duration audio recordings. \n\n',...
  'Kolbeinn Karlsson, Cornell University \n',...
  'Nicholas Bruns, Cornell University \n\n',...
  '============================================================',...
  '===============\n\n'];
fprintf(file,fileheader);

%Print parameters
fprintf(file,'PARAMETERS: \nC: %f \nrbf_sigma: %f \n',C,rbf_sigma);
fprintf(file,'Parameters optimized: %d \n',optimize_parameters);

%Data on training and test sets
fprintf(file,'\nTRAINING AND TEST SET INFO:\n');
fprintf(file,'Training set size: %d\n', train_size);
fprintf(file,'Training set positives: %d\n', train_pos);
fprintf(file,'Training set negatives: %d\n', train_neg);
fprintf(file,'Test set size: %d\n', test_size);
fprintf(file,'Test set positives: %d\n', test_pos);
fprintf(file,'Test set negatives: %d\n', test_neg);

%Now for the performance metrics
fprintf(file,'\nPERFORMANCE METRICS:\n');
fprintf(file,'True negatives: %d\n',true_neg);
fprintf(file,'True positives: %d\n',true_pos);
fprintf(file,'False negatives: %d\n',false_neg);
fprintf(file,'False positives: %d\n',false_pos);
fprintf(file,'Misclassification rate: %f\n',misclass);
fprintf(file,'Precision: %f\n',precision);
fprintf(file,'Recall: %f\n',recall);
fprintf(file,'F1 Score: %f\n',f1score);

%Timestamp and write elapsed time
fprintf(file,'\nCurrent date and time: %s\n',datestr(clock));
fprintf(file,'Elapsed time (seconds): %f\n',elapsed_time);

fclose(file);

%aaaaaaaaand we're done!




